#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <sys/time.h>
#include <sys/resource.h>

#define GPIO_BANK 1

#define BTN_OFF 0x80
#define LONG_PRESS 0x40

#define BUTTON_RED 1
#define BUTTON_GREEN 2
#define BUTTON_BLUE 3
#define BUTTON_CLEAR 4
#define BUTTON_BLACK 5
#define BUTTON_WHITE 6

#define LONG_PRESS_TIME 50

#ifdef GPIO_SIM

#define REGFILE "gpio_dbgregs"
#define GPIO_BASE 0
#define MAP_FLAGS MAP_SHARED

#else

#define REGFILE "/dev/mem"
#define GPIO_BASE  (0x200000)
#define PWM_BASE   (0x20C000)
#define CLK_BASE   (0x101000)
#define SYST_BASE  (0x003000)
#define MAP_FLAGS MAP_SHARED | MAP_LOCKED

#endif

#define GPIO_FSEL_REG(gpio) ((gpio) / 10)
#define GPIO_FSEL_SHIFT(gpio) (((gpio) % 10) * 3)
#define GPIO_FSEL_MASK(gpio) (7 << GPIO_FSEL_SHIFT(gpio))
#define GPIO_BIT(gpio) (1 << (gpio))

#define GPSET0 7
#define GPSET1 8
#define GPCLR0 10
#define GPCLR1 11
#define GPLEV0 13
#define GPLEV1 14
#define GPPUD 37
#define GPPUDCLK0 38
#define GPPUDCLK1 39

#define BCM_PASSWD (0x5A<<24)

#define PWM_CTL      0
#define PWM_STA      1
#define PWM_DMAC     2
#define PWM_RNG1     4
#define PWM_DAT1     5
#define PWM_FIFO     6
#define PWM_RNG2     8
#define PWM_DAT2     9

#define PWM_CTL_MSEN2 (1<<15)
#define PWM_CTL_PWEN2 (1<<8)
#define PWM_CTL_MSEN1 (1<<7)
#define PWM_CTL_CLRF1 (1<<6)
#define PWM_CTL_USEF1 (1<<5)
#define PWM_CTL_MODE1 (1<<1)
#define PWM_CTL_PWEN1 (1<<0)


#define CLK_CTL_BUSY    (1 <<7)
#define CLK_CTL_KILL    (1 <<5)
#define CLK_CTL_ENAB    (1 <<4)
#define CLK_CTL_SRC(x) ((x)<<0)

#define CLK_SRCS 2

#define CLK_CTL_SRC_OSC  1
#define CLK_CTL_SRC_PLLD 6

#define CLK_OSC_FREQ   19200000
#define CLK_PLLD_FREQ 500000000


#define CLK_GP0_CTL 28
#define CLK_GP0_DIV 29
#define CLK_GP1_CTL 30
#define CLK_GP1_DIV 31
#define CLK_GP2_CTL 32
#define CLK_GP2_DIV 33

#define CLK_PCMCTL 38
#define CLK_PCMDIV 39

#define CLK_PWMCTL 40
#define CLK_PWMDIV 41

#define SYST_CS  0
#define SYST_CLO 1
#define SYST_CHI 2




/* Set the function of a single GPIO. */
#define MODE_SET_1(gpio1, mode1) gpio_reg[GPIO_FSEL_REG(gpio1)] =      \
                                 (gpio_reg[GPIO_FSEL_REG(gpio1)]       \
                                  & ~(GPIO_FSEL_MASK(gpio1)))          \
                                 | ((mode1) << GPIO_FSEL_SHIFT(gpio1))

/* Set the function of two GPIOs in the same function register */
#define MODE_SET_2(gpio1, mode1, gpio2, mode2) gpio_reg[GPIO_FSEL_REG(gpio1)] =                     \
                                               (gpio_reg[GPIO_FSEL_REG(gpio1)]                      \
                                                & ~(GPIO_FSEL_MASK(gpio1) | GPIO_FSEL_MASK(gpio2))) \
                                               | ((mode1) << GPIO_FSEL_SHIFT(gpio1))                \
                                               | ((mode2) << GPIO_FSEL_SHIFT(gpio2))

#ifdef GPIO_DBG
static inline void send_evt(uint8_t c) {
    printf("---EVENT %02x\n", c);
}
#else
static inline void send_evt(uint8_t c) {
    write(1, &c, 1);
}
#endif

#ifdef GPIO_SIM

#define GPIO_R0 4
#define GPIO_R1 5
#define GPIO_A 0
#define GPIO_B 1
#define GPIO_C 2

#define GPIO_BEEP 12

#define INIT_GPIO_MODE() (void)0

#else /* ! GPIO_SIM */

#if GPIO_BANK == 1

#define GPIO_R0 19
#define GPIO_R1 26
#define GPIO_A 13
#define GPIO_B 6
#define GPIO_C 5

#define GPIO_BEEP 18

#define UPDATE_FUNC(cycle) MODE_SET_2(GPIO_C, cycle == 3 ? 1 : 0, GPIO_B, cycle == 2 ? 1 : 0); MODE_SET_1(GPIO_A, cycle == 1 ? 1 : 0)
#define INIT_GPIO_MODE() MODE_SET_2(GPIO_R0, 0, GPIO_R1, 0)
#elif GPIO_BANK == 2

#define GPIO_R0 23
#define GPIO_R1 24
#define GPIO_A 25
#define GPIO_B 8
#define GPIO_C 7

#define GPIO_BEEP 18



#define UPDATE_FUNC(cycle) MODE_SET_2(GPIO_B, cycle == 2 ? 1 : 0, GPIO_C, cycle == 3 ? 1 : 0); MODE_SET_1(GPIO_A, cycle == 1 ? 1 : 0)
#define INIT_GPIO_MODE() MODE_SET_2(GPIO_R0, 0, GPIO_R1, 0)

#endif /* GPIO_BANK */

#endif /* GPIO_SIM */

typedef struct {
    uint32_t reexec_ctr;
    uint32_t readhead;
    uint32_t writehead;
    uint32_t beep_pattern[128];
} poller_shmem_t;

static volatile uint32_t* gpio_reg;
static volatile uint32_t* pwm_reg;
static volatile uint32_t* clk_reg;
static volatile uint32_t* syst_reg;

static volatile poller_shmem_t* gpio_poller;
static uint32_t last_beep_ctr;
static uint32_t beep_remain;

void busy_wait(uint32_t micros) {
    uint32_t start = syst_reg[SYST_CLO];
    do { /* nothing */ } while ((syst_reg[SYST_CLO] - start) <= micros);
}

void process_rotor(uint32_t reg) {
    static int last_rotor = 0;
    int r0 = (reg >> GPIO_R0) & 1;
    int r1 = (reg >> GPIO_R1) & 1;

    int rotor = (r1 << 1) | (r0 ^ r1);
    int diff = (rotor - last_rotor) & 3;
    last_rotor = rotor;
    if (diff == 1) {
        send_evt(0x20);
    } else if (diff == 3) {
        send_evt(BTN_OFF | 0x20);
    }
}

void process_button(uint32_t reg, int cycle) {
    static uint16_t lastpoll = 0;
    static int curbtn = 0;
    static int long_press_count = 0;
    /* Convert the button GPIO pins into a 3-bit value. This fails if GPIO_B
     * is 0 or GPIO_C is 0 or 1, but those GPIOs are unusable on the Pi
     * anyway */

    uint16_t curpoll = ((reg >> GPIO_A) & 1) | ((reg >> (GPIO_B-1)) & 2) | ((reg >> (GPIO_C-2)) & 4);

    /* Mask out the bit that is currently being pulled low */
    curpoll &= ~((1 << cycle) >> 1);

    /* Shift it into the nybble for the current cycle */
    uint16_t newpoll = ( lastpoll & ~(15 << (cycle << 2)) ) | (curpoll << (cycle << 2));

#ifdef GPIO_DBG
    printf("%d %08x cp=%04x np=%04x lp=%04x\n", cycle, reg, curpoll, newpoll, lastpoll);
#endif
    if (newpoll != lastpoll) {
        lastpoll = newpoll;
        int newbtn = curbtn;
        switch(newpoll) {
            case 0x0000: newbtn = 0; break;
            case 0x1101: newbtn = BUTTON_CLEAR; break; /* AG */
            case 0x2022: newbtn = BUTTON_WHITE; break; /* BG */
            case 0x0444: newbtn = BUTTON_RED; break;   /* CG */
            case 0x1040: newbtn = BUTTON_BLUE; break;  /* AC */
            case 0x0120: newbtn = BUTTON_BLACK; break; /* AB */
            case 0x2400: newbtn = BUTTON_GREEN; break; /* BC */
        }
        if (newbtn != curbtn) {
            if (curbtn != 0) {
                if (long_press_count == 0) {
                    send_evt(BTN_OFF | LONG_PRESS | curbtn);
                } else {
                    send_evt(BTN_OFF | curbtn);
                }
            }
            if (newbtn != 0) {
                send_evt(newbtn);
            }
            curbtn = newbtn;
            long_press_count = LONG_PRESS_TIME;
        }
    }
    if (curbtn != 0) {
        if (long_press_count > 0) {
            long_press_count--;
            if (long_press_count == 0) {
                send_evt(LONG_PRESS | curbtn);
            }
        }

    }
}

void process_beep() {

    uint32_t readhead = gpio_poller->readhead;
    uint32_t writehead = gpio_poller->writehead;
    if (readhead == writehead || readhead > 0x7f)
        return;

    uint32_t freq_dur = gpio_poller->beep_pattern[readhead];
    int freq = (freq_dur >> 16);
    int dur = (freq_dur & 0xFFFF);

    if (freq == 0) {
        pwm_reg[PWM_CTL] &= ~PWM_CTL_PWEN1;
        gpio_poller->beep_pattern[readhead] |= 0xFFFF0000;
    } else if (freq != 0xFFFF) {
        uint32_t range = (uint32_t)(CLK_PLLD_FREQ / (2.0 * freq) + 0.5);
        uint32_t dutycycle = range >> 1;
        pwm_reg[PWM_RNG1] = range;
        pwm_reg[PWM_DAT1] = dutycycle;
        pwm_reg[PWM_CTL] = PWM_CTL_PWEN1 | PWM_CTL_MSEN1;
        gpio_poller->beep_pattern[readhead] |= 0xFFFF0000;
    } else {
        dur -= 5;
        if (dur <= 0) {
            gpio_poller->beep_pattern[readhead] = 0xFFFF0000;
            dur = 0;
            gpio_poller->readhead = readhead = (readhead + 1) & 0x7F;
            if (readhead == writehead) {
                pwm_reg[PWM_CTL] &= ~PWM_CTL_PWEN1;
                return;
            }
        } else {
            gpio_poller->beep_pattern[readhead] = 0xFFFF0000 | dur;
        }
    }
}

int is_bcm2835() {
    FILE* fp = fopen("/proc/cpuinfo", "r");
    char buf[512];

    if (!fp) {
        perror("cpuinfo");
        return -1;
    }
    while (fgets(buf, sizeof(buf), fp) != NULL) {
        if (!strncmp("model name", buf, 10)) {
            return strstr (buf, "ARMv6") != NULL;
        }
    }
    return -1;
}

int main(int argc, char** argv) {
    int curbtn = 0;
    int long_press_count = 1;

    int is2835 = is_bcm2835();
    if (is2835 == -1) {
        fprintf(stderr, "Could not determine Pi model\n");
        return 1;
    }

    uint32_t periph_base = is2835 ? 0x20000000 : 0x3F000000;
    fprintf(stderr, "periph_base = %08x\n", periph_base);

#ifndef GPIO_DBG

    int memfd = open(REGFILE, O_RDWR);
    if (memfd < 0) {
        perror("Can't open " REGFILE);
        return 1;
    }

#define MAP_REGS(var, typ, len)                                         \
    var = mmap(0, 0xB4, PROT_READ | PROT_WRITE,                         \
                    MAP_FLAGS, memfd, periph_base + typ ## _BASE);      \
    if (var == MAP_FAILED) {                                            \
        perror("Failed to mmap " #typ " regs");                         \
        return 1;                                                       \
    }

    MAP_REGS(gpio_reg, GPIO, 0xB4);
    MAP_REGS(pwm_reg, PWM, 0x28);
    MAP_REGS(clk_reg, CLK, 0xA8);
    MAP_REGS(syst_reg, SYST, 0x1C);

#else

    gpio_reg = malloc(0xB4 * 4);
    memset(gpio_reg, 0, 0xB4 * 4);

#endif

    int beepfd = open("/dev/shm/gpio_poller", O_RDWR | O_CREAT, 0644);
    if (beepfd < 0) {
        perror("Can't open gpio_poller shared memory");
        return 1;
    }

    ftruncate(beepfd, sizeof(poller_shmem_t));
    gpio_poller = (poller_shmem_t*)mmap(0, sizeof(poller_shmem_t), PROT_READ | PROT_WRITE, MAP_FLAGS, beepfd, 0);
    if (gpio_poller == MAP_FAILED) {
        perror("Failed to mmap gpio_poller shared memory");
        return 1;
    }
    uint32_t exec_ctr = gpio_poller->reexec_ctr;

   /* Set output register. All of our GPIOs are either pulled down or floating, so clear the output bits */

    gpio_reg[GPCLR0] = GPIO_BIT(GPIO_A) | GPIO_BIT(GPIO_B) | GPIO_BIT(GPIO_C) | GPIO_BIT(GPIO_R0) | GPIO_BIT(GPIO_R1) | GPIO_BIT(GPIO_BEEP);
    INIT_GPIO_MODE();

    /* Set up pull-up resistors.

       The following sequence of events is required:
       1. Write to GPPUD to set the required control signal (i.e. Pull-up or Pull-Down or
          neither to remove the current Pull-up/down)
       2. Wait 150 cycles – this provides the required set-up time for the control signal
       3. Write to GPPUDCLK0/1 to clock the control signal in to the GPIO pads you wish to
          modify – NOTE only the pads which receive a clock will be modified, all others
          will retain their previous state.
       4. Wait 150 cycles – this provides the required hold time for the control signal
       5. Write to GPPUD to remove the control signal
       6. Write to GPPUDCLK0/1 to remove the clock
     */

    gpio_reg[GPPUD] = 2;
    usleep(50);
    gpio_reg[GPPUDCLK0] = GPIO_BIT(GPIO_A) | GPIO_BIT(GPIO_B) | GPIO_BIT(GPIO_C) | GPIO_BIT(GPIO_R0) | GPIO_BIT(GPIO_R1);
    usleep(50);
    gpio_reg[GPPUD] = 0;
    gpio_reg[GPPUDCLK0] = 0;

    usleep(50);

#ifndef GPIO_SIM

    // Init clock regs for PWM

    while (clk_reg[CLK_PWMCTL] & CLK_CTL_BUSY) {
        clk_reg[CLK_PWMCTL] = BCM_PASSWD | (clk_reg[CLK_PWMCTL] & ~CLK_CTL_ENAB);
    }

    clk_reg[CLK_PWMDIV] = BCM_PASSWD | 0x2000;

    usleep(10);

    clk_reg[CLK_PWMCTL] = (BCM_PASSWD | CLK_CTL_SRC_PLLD);

    usleep(10);

    clk_reg[CLK_PWMCTL] |= (BCM_PASSWD | CLK_CTL_ENAB);


    // Init PWM regs
    pwm_reg[PWM_CTL] = 0;

    usleep(10);

    pwm_reg[PWM_STA] = -1;

    //usleep(10);

    /* enable PWM channel 1 and use fifo */

    //pwm_reg[PWM_CTL] = PWM_CTL_USEF1 | PWM_CTL_MODE1 | PWM_CTL_PWEN1;

    // Select alternate function 0 for gpio_poller pin (PWM0)
    MODE_SET_1(GPIO_BEEP, 2);

    setpriority(PRIO_PROCESS, 0, -20);

#endif

    fprintf(stderr, "gpio_poll started\n");

    int cycle = 0;
    while (1) {
        if (gpio_poller->reexec_ctr != exec_ctr) {
            execv(argv[0], argv);
        }

        uint32_t reg;
#ifdef GPIO_DBG
        char tmpbuf[64];
        int nr = scanf("%x", &reg);
        if (nr == EOF) break;
#elif defined(GPIO_SIM)
        usleep(10000);
        reg = gpio_reg[0] >> (cycle << 3);
#else
        UPDATE_FUNC(cycle);
        usleep(5000);
        reg = ~gpio_reg[GPLEV0];
#endif

        process_rotor(reg);
        process_beep();
        process_button(reg, cycle);
        cycle = (cycle + 1) & 3;




    }

    return 0;
}
